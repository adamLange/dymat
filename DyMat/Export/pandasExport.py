import pandas as pd

def export(dm,varList=None):
  if not varList:
    varList = dm.names()
  dfList = []
  blockDict = dm.sortByBlocks(varList)
  for block in blockDict.keys():
    df = pd.DataFrame()
    df['time'] = dm.abscissa(block)[0]
    for variableName in blockDict[block]:
      df[variableName] = dm[variableName]
    dfList.append(df)
  return dfList
  
